FROM python:3.9.7

COPY ["./app", "/app"]

WORKDIR /app

RUN ["python3", "-m", "pip", "install", "-r", "/app/requirements.txt"]

EXPOSE 5000

CMD ["flask", "run", "--debug", "--host", "0.0.0.0"]
