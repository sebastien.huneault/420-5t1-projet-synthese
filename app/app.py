from flask import Flask, request, render_template, session, g, flash, redirect, url_for, make_response, render_template_string

import hashlib
import importlib
from functools import wraps
import sqlite3
import os

app = Flask(__name__)
app.secret_key = 'abc123'
app.config['DATABASE'] = 'db.sqlite'
app.config['SESSION_COOKIE_HTTPONLY'] = False

# Les valeur de retour de chaque entrée de la base de donnée
def dict_factory(cursor, row):
    fields = [column[0] for column in cursor.description]
    return {key: value for key, value in zip(fields, row)}

# Connecter à la base de données
def get_db():
    if 'db' not in g:
        g.db = sqlite3.connect(app.config['DATABASE'])
        g.db.row_factory = dict_factory
    return g.db

# Initialiser la base de données
def init_db():
    db = sqlite3.connect(app.config['DATABASE'])
    with open('db.sql', 'r') as f:
        script = f.read()
        db.cursor().executescript(script)
    db.commit()

# Fermer la base de données lorsque l'application ferme
@app.teardown_appcontext
def close_db(e=None):
    db = g.pop('db', None)
    if db is not None:
        db.close()

# Initialiser la base de données
init_db()


# Utilitaires
def md5_encrypt(text):
    md5_hash = hashlib.md5()
    md5_hash.update(text.encode('utf-8'))
    encrypted_text = md5_hash.hexdigest()
    return encrypted_text



# Aide pour la connexion
def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if 'logged' not in session:
            return redirect(url_for('index'))
        return f(*args, **kwargs)
    return wrapper

# Les routes
@app.route('/')
def index():
    return render_template('page_accueil.html')

@app.route('/enregistrement', methods=['GET', 'POST'])
def enregistrement():
    if request.method == 'GET':
        return render_template('page_enregistrement.html')
    else:
        db = get_db()
        username = request.form['utilisateur']
        password = md5_encrypt(request.form['motpasse'])
        fullname = request.form['nom']
        bio = request.form['bio']

        # Validation
        if len(password) < 6:
            flash('Le mot de passe doit être plus grand que 6 caractères', 'danger')
            return redirect(url_for('enregistrement'))
        # Vérifier les doublons
        cursor = db.execute(f'SELECT username FROM users WHERE username = "{username}"')
        users = cursor.fetchall()
        if len(users) > 0:
            flash(f'L\'utilisateur {users[0]["username"]} existe déjà', 'danger')
            return redirect(url_for('enregistrement'))

        # Ajouter l'utilisateur
        cursor = db.execute(f'INSERT INTO users (username, password, fullname, bio) VALUES (?, ?, ?, ?)', (username, password, fullname, bio))
        db.commit()

        flash('Vous êtes maintenant enregistré!', 'success')
        return redirect(url_for('enregistrement'))
    
@app.route('/utilisateurs')
def utilisateurs():
    if request.method == 'GET':
        db = get_db()
        users = db.execute('SELECT id, username, fullname, bio FROM users').fetchall()
        for user in users:
            user['bio'] = render_template_string(user['bio'])
        return render_template('page_utilisateurs.html', users=users)
    
@app.route('/utilisateurs/<int:utilisateur>', methods=['DELETE'])
@login_required
def utilisateurs_delete(utilisateur):
    db = get_db()
    db.execute('DELETE FROM users WHERE id = ?', (utilisateur, ))
    db.commit()
    return make_response('OK', 200)

@app.route('/connexion', methods=['GET', 'POST'])
def connexion():
    if request.method == 'GET':
        return render_template('page_connexion.html')
    else:
        db = get_db()
        username = request.form['utilisateur']
        password = md5_encrypt(request.form['motpasse'])
        user = db.execute('SELECT id, username, fullname FROM users WHERE username = ? AND password = ?', (username, password)).fetchall()
        
        # Connexion avec succès
        if len(user) > 0:
            session['logged'] = True
            session['isAdmin'] = (username == 'admin')
            session['user_id'] = user[0]['id']
            session['username'] = user[0]['username']
            session['fullname'] = user[0]['fullname']
            flash('Vous êtes connecté!', 'success')
            return redirect(url_for('index'))
        else:
            session.clear()
            flash('Mauvais utilisateur ou mauvais mot de passe', 'danger')
            return redirect(url_for('connexion'))

@app.route('/deconnexion')        
def deconnexion():
    session.clear()
    return redirect(url_for('index'))

@app.route('/plugins', methods=['GET', 'POST'])
@login_required
def plugins():
    plugins = [f for f in os.listdir('./plugins') if f.endswith('.py') ]
    if request.method == 'GET':
        return render_template('page_plugins.html', plugins=plugins)
    else:
        plugin = request.form['plugin']
        module_name = '.'.join(plugin.split('.')[:-1])

        try:
            tmp = importlib.import_module(f'plugins.{module_name}')
            resultat = tmp.execution()
            return render_template('page_plugins.html', plugins=plugins, resultat=resultat)
        except:
            with open(f'./plugins/{plugin}') as f:
                code = f.read()
            flash(f'Erreur! Le plugin n\'a pu être exécuté...\n\n{code}', 'danger')
            return redirect(url_for('plugins'))

@app.route('/plugins/ajouter', methods=['POST'])
@login_required
def plugins_ajouter():
    file = request.files['plugin']
    # Fichier python seulement
    if not file.filename.lower().endswith('.py'):
        flash('Seuls les fichiers .py sont acceptés', 'danger')
        return redirect(url_for('plugins'))
    
    file.save(f'./plugins/{file.filename}')
    flash('Le plugin a été ajouté avec succès', 'success')
    return redirect(url_for('plugins'))

if __name__ == '__main__':
    app.run(debug=True)

