-- schema.sql

DROP TABLE IF EXISTS users;

CREATE TABLE users (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    fullname TEXT NOT NULL,
    bio TEXT
);

INSERT INTO users (username, password, fullname, bio) VALUES
    ('admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrateur', 'Je suis <strong>l''administrateur</strong>!')
