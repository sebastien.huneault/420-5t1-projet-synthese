# Projet final
Pour démarrer le projet :
```bash
git clone https://gitlab.com/sebastien.huneault/420-5t1-projet-synthese
cd 420-5t1-projet-synthese
docker-compose up
```
L'application sera accessible à l'adresse http://localhost:5000
